var express = require('express');
var router = express.Router();
var controller = require('../controllers/admin');

// Require admin authentication for all requests.
router.use(controller.auth);

// Run MOSS plagiarism detection script.
router.get("/:course/:assignment/moss", controller.moss);

// Run ETector plagiarism detection script.
router.get("/:course/:assignment/etector", controller.etector);

// Archive of all files for an assignment.
router.get("/:course-:assignment.zip", controller.archive);

// List of assignments for specified course.
router.get("/:course", controller.assignments);

// List of courses.
router.get("/", controller.courses);

module.exports = router;
