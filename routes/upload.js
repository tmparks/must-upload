var express = require('express');
var router = express.Router();
var controller = require('../controllers/upload');

// GET previously uploaded file
router.get("/:course/:assignment/:file", controller.download);

// GET upload form
router.get("/:course/:assignment", controller.form);

// POST upload form
router.post("/:course/:assignment", controller.multer.any(), controller.upload);

// GET list of assignments for specified course
router.get("/:course", controller.assignments);

// GET list of courses
router.get("/", controller.courses);

module.exports = router;
