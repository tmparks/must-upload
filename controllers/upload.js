var mkdirp = require('mkdirp');
var multer = require('multer');
var path = require('path');
var spawn = require('child_process').spawn;
var config = require('../config/upload');

// Destination directory for uploaded files.
// ../../data is compatible with OpenShift
function upload_destination(req) {
  return path.join(__dirname, '..', '..', 'data',
    req.params.course, req.params.assignment, req.user);
}

// Destination and filename for storage.
var storage = multer.diskStorage({
  destination: function(req, file, callback) {
    var dest = upload_destination(req);
    mkdirp(dest, function(err) {
      if (err) {
        console.error(err);
      }
      callback(null, dest);
    });
  },
  filename: function(req, file, callback) {
    callback(null, file.originalname);
  }
});

// Filter for uploaded files.
module.exports.multer = multer({
  storage: storage,
  fileFilter: function(req, file, callback) {
    if (file.originalname === file.fieldname) {
      callback(null, true);
    }
    else {
      callback(new Error('File name must be ' + file.fieldname));
    }
  }
});

// Download a file.
module.exports.download = function(req, res, next) {
  res.sendFile(req.params.file, {root: upload_destination(req)},
    function(err) {
      if (err) {
        console.error(err);
        next(); // 404 not found
      }
    });
};

// Upload file(s).
module.exports.upload = function(req, res, next) {
  console.log(req.files);
  var child = spawn(path.join(__dirname, '..', 'bin', 'post-upload.sh'), [
    path.join(__dirname, '..'),
    upload_destination(req),
    req.params.course,
    req.params.assignment,
    req.user,
    req.files.length
  ]);
  child.stdout.pipe(res);
};

// Upload form for an assignment.
module.exports.form = function(req, res, next) {
  var course = config[req.params.course];
  if (!course) {
    next(); // Not found.
  }
  else {
    var assignment = course[req.params.assignment];
    if (!assignment) {
      next(); // Not found.
    }
    else {
      assignment.assignment = req.params.assignment;
      res.render('upload', assignment); // Render form.
    }
  }
};

// List of assignments.
module.exports.assignments = function(req, res, next) {
  res.render('upload_assignments', {
    title: req.params.course,
    course: req.params.course,
    config: config
  })
}

// List of courses.
module.exports.courses = function(req, res, next) {
  res.render('courses', {
    title: 'MUST Assignment Upload',
    base: 'upload',
    config: config
  });
};
