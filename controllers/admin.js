var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
var path = require('path');

var auth = require('../config/auth');
var upload = require('../config/upload');

// Execute a command and redirect to the URL at the end of stdout.
function exec_redirect(command, title, req, res, next) {
  exec(command, function(err, stdout, stderr) {
    if (err) {
      next(err);
    }
    else {
      res.render('results', {
        title: title,
        url: stdout.trim().split(/\s+/).pop(),
        output: stdout
      });
    }
  });
}

// Authentication.
module.exports.auth = function(req, res, next) {
  if (auth.admins.indexOf(req.user) < 0) {
    var message = 'Unauthorized: ' + req.user + ' is not an administrator';
    console.error(message);
    var err = new Error(message);
    err.status = 403;
    next(err);
  }
  else {
    next();
  }
};

// MOSS plagiarism detection script.
module.exports.moss = function(req, res, next) {
  var root = path.join(__dirname, '..');
  var command = 'cd ' + path.join(root, '..', 'data',
    req.params.course) + '; ';
  command += path.join(root, 'bin', 'moss.pl');
  command += ' -l java';
  command += ' -c ' + req.params.course + '-' + req.params.assignment;
  command += ' -d';
  var base = '';
  var input = '';
  var files = upload[req.params.course][req.params.assignment].files;
  files.forEach(function(value, index, array) {
    base += ' -b ' + path.join(req.params.assignment, req.user, value);
    input += ' ' + path.join(req.params.assignment, '*', value);
  });
  command += base;
  command += input;
  exec_redirect(command, 'MOSS Results', req, res, next);
};

// ETector plagiarism detection script.
module.exports.etector = function(req, res, next) {
  var root = path.join(__dirname, '..');
  var command = 'cd ' + path.join(root, '..', 'data',
    req.params.course) + '; ';
  command += path.join(root, 'bin', 'etector.py');
  command += ' --join=0';
  command += ' --tag=' + req.params.course + '-' + req.params.assignment;
  command += ' --background=' + path.join(req.params.assignment, req.user);
  command += ' --current=' + req.params.assignment;
  exec_redirect(command, 'ETector Results', req, res, next);
};

// Archive of all files for an assignment
module.exports.archive = function(req, res, next) {
  var child = spawn('zip', ['--recurse-paths', '-', '.'],
    {
      cwd: path.join(__dirname, '..', '..', 'data',
        req.params.course, req.params.assignment)
    });
  child.on('error', function(err) {
    console.error(err);
  });
  if (child.pid) {
    res.attachment();
    res.type('zip');
    child.stdout.pipe(res);
  }
  else {
    next(new Error('Cannot Create Archive'));
  }
};

// List of assignments.
module.exports.assignments = function(req, res, next) {
  res.render('admin_assignments', {
    title: req.params.course,
    course: req.params.course,
    config: upload
  });
};

// List of courses.
module.exports.courses = function(req, res, next) {
  res.render('courses', {
    title: 'MUST Plagiarism Detection',
    base: 'admin',
    config: upload
  });
};
