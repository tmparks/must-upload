var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

// Authentication
var passport = require('passport');
var strategy = require('passport-google-oauth').OAuth2Strategy;
var session = require('express-session');
var Store = require('session-file-store')(session);
var auth = require('./config/auth');

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(obj, done) {
    done(null, obj);
});

passport.use(new strategy(auth.google,
    function(accessToken, refreshToken, profile, done) {
        var email = profile.emails[0].value;    // First email address.
        var user = email.split('@')[0];         // User part of address.
        if (auth.users.indexOf(email) < 0) {
            console.log('Unsucessful sign in attempt by ' + email);
            return done(null, false,
                {message: 'Unauthorized user: ' + email});
        }
        else {
            console.log('User ' + user + ' signed in.');
            return done(null, user);
        }
    }));

var routes = require('./routes/index');
var users = require('./routes/users');
var upload = require('./routes/upload');
var admin = require('./routes/admin');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('common'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
    store: new Store(),
    secret: 'oxHjd0V5ssz1JZdaN6iC',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

// Respond to OpenShift application health check
app.get('/health', function(req, res) {
  res.sendStatus(200);
});

// Make authenticated user available in all views.
app.use(function(req, res, next) {
    res.locals.user = req.user;
    next();
});

app.get('/login', function(req, res) {
    res.render('login');
});

app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

app.get('/auth/google', passport.authenticate('google', {
    scope: ['email'],
    prompt: 'select_account'
}));

app.get('/auth/google/callback', function(req, res, next) {
    passport.authenticate('google', function(err, user, info) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.render('login', {message: info.message});
        }
        req.login(user, function(err) {
            if (err) {
                return next(err);
            }
            var redirect = '/';
            if (req.session.returnTo) {
                redirect = req.session.returnTo;
                delete req.session.returnTo;
            }
            return res.redirect(redirect);
        });
    })(req, res, next);
});

// Require authentication for all other requests.
app.all('*', function(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    // Return here after successful authentication.
    req.session.returnTo = req.path;
    res.redirect('/login');
});

app.use('/', routes);
app.use('/users', users);
app.use('/upload', upload);
app.use('/admin', admin);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
