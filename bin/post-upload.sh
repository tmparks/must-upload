#!/bin/bash
base=$1
destination=$2
course=$3
assignment=$4
user=$5
count=$6
zone=Africa/Kampala

if [ ! -d $destination ]
then
    echo No files have been uploaded.
    exit
fi

cd $destination

# List all files in the destination directory.
if [ $count -eq 1 ]
then
    echo $count new file was uploaded.
else
    echo $count new files were uploaded.
fi
echo
echo The following files are present in the upload destination.
echo All times are in the $zone time zone.
echo
echo -e "Date        Time   Size\tFile"
echo -e "----        ----   ----\t----"
for file in *
do
    set -- $(TZ=$zone ls -hlt --time-style=long-iso "$file")
    size=$5
    date=$6
    time=$7
    echo -e "$date  $time  $size\t$file"
done
echo

# Commit file(s) to git repository.
{
    date
    echo $course $assignment $user
    git add --all
    git status
    git commit -m "$course $assignment $user"
    echo
} >> $base/git.log 2>&1
