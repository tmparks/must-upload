#!/usr/bin/python

# your course key - do not modify this line
# and do not share this file/key beyond your administrative team
accountkey = "f03ca49e1698bd88aaa3f8a677605c96"

# list the match criteria for any files in the directories specified,
# and also for any files to be extracted from any tar files specified.
# multiple match patterns can be separated by spaces or commas. The
# format of these is similar to Unix shells, rather than regular
# expressions. So, to match anything, use just * instead of .*
# matches do not cross directories - all slashes must be specified.
# can also be given on the command line via -m or --match
match = "*.java,*/*.java,*/*/*.java,*/*/*/*.java"
match += ",*.html,*/*.html,*/*/*.html,*/*/*/*.html"


# list the tar files or directories for the current semester or term,
# separating items by commas. any matches found in these files will be
# included in the report. For both the tar files and the directories,
# only files that match the specified pattern(s) will be included in
# the comparison.
# can also be given on the command line via -c or --current
current = ""


# list the tar files or directories for any previous semesters or
# term, separating items by commas.  these will be checked for matches
# against the current term, but any matches that are solely between
# previous terms will not be included in the report. For both the tar
# files and the directories, only files that match the specified
# pattern(s) will be included in the comparison.
# can also be given on the command line via -p or --previous
previous = ""


# the background materials for the course - any content from this will
# not be included in a match. if any tar files are listed, all of the
# content of the file will be included, regardless of the match
# pattern specified.
# can also be given on the command line via -b or --background
background = ""


# put names of any course staff here - any file that has a directory
# component that matches one of these names will be ignored. Put
# multiple names as a comma-separated list with no spaces.
# can also be given on the command line via -s or --staff
staff = ""


# the maximum number of items to include in the report.
# can also be given on the command line via -l or --limit
limit = "50"


# if set, any input files within the same final directory will be
# treated as part of one file. useful when a student submits multiple
# files for an assignment.
# can also be given on the command line via -j (no parameters) or --join (0 or 1)
join = "1"


# if set, the tag for this run when it is being displayed in a listing.
# any underscores will be displayed as spaces in the listing.
# can also be given on the command line via -t or --tag
tag = ""


# the detection level can be a number between 1 and 9 inclusive, with
# 1 being the fastest, and 9 being the most thorough.  only modify
# this number if you really care about run speed.  setting it to 5 is
# a reasonable compromise between speed and thoroughness. you can also
# set it to the word "structure" to run structure-only detection,
# wherein all comments and variable names are ignored.
# can also be given on the command line via -d or --detection
detection = "9"


# the time limit is the maximum time the program will spend on
# detection calculations, which does not include input or output.
# The server may internally cap this value if it is too large.
timelimit = "300"


# if set, suppresses self-plagiarism reports. these can occur when a
# student drops a course and later re-takes it, re-using old
# assignments
noselfplag = "1"


# when many students copy from the same source, over time the archive
# will contain lots of similar-looking code, and this may be
# interpreted as being commonly-used code, which gets ignored. To
# avoid this, when many students copy from the same source, add their
# usernames to the list of fishy code. Code from these users will not
# be counted when determining what code is commonly-used
fishy = ""


# if background files are specified, determine how aggressively they
# are used to strike out potential matches. the value is how long a
# match with the background file is required before the text is struck
# from possible matches. smaller values will yield more matches, so be
# careful lowering this value. May be clipped if set too high. using
# a value of zero disables explicit striking-out of template matches.
# can also be given on the command line via -u or --untemplate
untemplate = "10"


# if too many tokens are matched it looks cluttered, so allow some
# degree of pruning. Zero disables pruning, with values of 1 to 9
# specifying degree of pruning, with 9 being very aggressive. 
# can also be given on the command line via -r or --removeclutter
removeclutter = "0"

#
# ------------------ DO NOT MODIFY ANYTHING BELOW HERE --------------
#


import glob
import os
import socket
import sys
import hashlib
import getopt
import time

totalFilesToSend = 0
numFilesSent = 0

version = ""

# -----------------------------------------------------------------------
def EasySplit(x):
    split1 = x.split(",")
    split2 = []
    for i in split1:
        split2 += i.split(" ")
    split3 = []
    for i in split2:
        if len(i) != 0:
            split3 += [i]
    return split3
# -----------------------------------------------------------------------
def FindAllMatches(dirsOrTars):
    split = EasySplit(dirsOrTars)
    matches = EasySplit(match)
    files = []

    for f in split:
        if f.find("*") != -1:
            dirs = glob.glob(f)
            for d in dirs:
                files += FindAllMatches(d)        
        elif os.path.isfile(f):
            files += [f]
        elif os.path.isdir(f):
            for m in matches:
                files += glob.glob(f + m)
    return files
# -----------------------------------------------------------------------
def SendAndReceive(sock, msg):
    total = 0
    target = len(msg)
    while total < target:
        bytes = sock.send(msg[total:])
        if bytes == 0:
            print "socket died in send"
            sys.exit()
        total += bytes
    reply = sock.recv(4096)
    words = reply.split(" ")
    return words
# -----------------------------------------------------------------------
def SendFileList(listType, files):
    for fname in files:
        fp = open(fname, 'r')
        contents = fp.read()
        fp.close()
        if len(contents) > 1:
            m = hashlib.md5()
            m.update(contents)
            digest = m.hexdigest()
        else:
            digest = "0"
        msg = listType +  " " + str(len(contents)) + " " + digest + " " + fname
        global numFilesSent
        global totalFilesToSend
        numFilesSent += 1
        print "sending file", str(numFilesSent)+"/"+str(totalFilesToSend), fname
        reply = SendAndReceive(s, str(len(msg)) + " " + msg)
        if reply[0] != "send" and reply[0] != "skip":
            print " ".join(reply)
            sys.exit()
        if reply[0] == "send":
            reply = SendAndReceive(s, str(len(contents)) + " " + contents)
            if reply[0] != "ok":
                print " ".join(reply)
                sys.exit()
# -----------------------------------------------------------------------


opts, args = getopt.getopt(sys.argv[1:], "b:c:d:f:jl:m:p:r:s:t:u:v:", ["background=", "current=", "detection=", "join=", "limit=", "match=", "previous=", "removeclutter=", "staff=", "tag=", "untemplate=", "version="])

for o, a in opts:
    if o in ("-b", "--background"):
        background = a
    elif o in ("-c", "--current"):
        current = a
    elif o in ("-d", "--detection"):
        detection = a
    elif o in ("-f", "--fishy"):
        fishy = a
    elif o == "-j":
        join = "1";
    elif o == "--join":
        join = a;
    elif o in ("-l", "--limit"):
        limit = a
    elif o in ("-m", "--match"):
        match = a
    elif o in ("-p", "--previous"):
        previous = a
    elif o in ("-r", "--removeclutter"):
        removeclutter = a
    elif o in ("-s", "--staff"):
        staff = a
    elif o in ("-t", "--tag"):
        tag = a
    elif o in ("-u", "--untemplate"):
        untemplate = a
    elif o in ("-v", "--version"):
        version = a

# first get rid of any spaces
match = match.replace(" ", ",")
current = current.replace(" ", ",")
previous = previous.replace(" ", ",")
background = background.replace(" ", ",")
staff = staff.replace(" ", ",")
fishy = fishy.replace(" ", ",")

print "searching for matching files"
currFiles = FindAllMatches(current)
#print files
print len(currFiles), " current files found"

backFiles = FindAllMatches(background)
#print files
print len(backFiles), " background files found"

prevFiles = FindAllMatches(previous)
#print files
print len(prevFiles), " previous files found"

print len(currFiles), "current files,", len(prevFiles), "previous files,", len(backFiles), "background files"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(("www.etector.org", 3141))


# send protocol version and account key
# read response, check for ok

print "sending account info"
reply = SendAndReceive(s, "2 " + accountkey + "\n")
if reply[0] != "ok":
    print " ".join(reply)
    sys.exit()

ltime = time.strftime("%I:%M%p %Z, %a %b %d, %Y ")
ltimer = ltime.replace(" ", "_")

# send match string, staff string, and limit string
# read response, check for ok
print "sending parameters"
tagr = tag.replace(" ", "_")
msg = "match=" + match + " staff=" + staff + " join=" + join + " limit=" + limit + " tag=" + tagr + " ltime=" + ltimer + " version=" + version + " detection=" + detection + " timelimit=" + timelimit + " noselfplag=" + noselfplag + " untemplate=" + untemplate + " removeclutter=" + removeclutter + " fishy=" + fishy
reply = SendAndReceive(s, str(len(msg)) + " " + msg)
if reply[0] != "ok":
    print " ".join(reply)
    sys.exit()

# loop through all files to send
#   for each file, send the following:
#     file type (curr, prev, back), file size, md5sum, file name
#     wait for response - ok, skip, exit
#     if ok, send file

totalFilesToSend = len(backFiles) + len(currFiles) + len(prevFiles)

SendFileList("back", backFiles)
SendFileList("curr", currFiles)
SendFileList("prev", prevFiles)

print len(currFiles), "current files,", len(prevFiles), "previous files,", len(backFiles), "background files"

# send done message, wait for response
# print response to user

msg = "done"
reply = SendAndReceive(s, str(len(msg)) + " " + msg)
if reply[0] != "ok":
    print " ".join(reply)
    sys.exit()

print "all files sent - waiting on server"
while True:
    reply = s.recv(4096)
    if reply == '':
        break
    sys.stdout.write(reply)



# -----------------------------------------------------------------------
